# Physiological Measurements

This project contains MATLAB script to extract pulse oximetry and respiration traces during the functional MRI signal collection and then compute several common physiological measurements using these physiological traces. The pulse oximetry and respiration signals are recorded from finger transducer and chest belt respectively using Simens Advanced fMRI package WIP package. 

First, download the related scripts (cal_physio_measure.m and FT_Filter_wDC.m). Aslo, download a toolbox (dicm2nii) from Dr. Xiangrui Li at the website (https://www.mathworks.com/matlabcentral/fileexchange/42997-xiangruili-dicm2nii). 

Second, adding these scripts to MATLAB path.

Then you can apply the script (cal_physio_measure.m) to calculate physiological measurements, including heart rate, respiratory volume, and photoplethysmography (PPG) amplitude. The input to this script is the DICOM file generated using the script above. An example DICOM file (6434RP_1_s007_trace.dcm) was uploaded. 

- _Heart rate: hwo many times heart beats within one minute_

- _Respiratory volume: reflecting the volume of gas in the lungs_

- _Photoplethysmography (PPG) amplitude: reflecting blood volume and its pulsatile variations with the cardiac cycle (Shelley, Anesth. Analg., 2007)_





Setup (MATLAB):

The Signal and Curve Fitting toolboxes from MATLAB will be needed. Please intall these toolboxes from MATLAB if not.

Put cal_physio_measure.m and FT_Filter_wDC.m to the same path with DICOM file before running scripts to calculate physiological measurements. Or add this function to your own MATLAB path.

A toolbox (dicm2nii) from Dr. Xiangrui Li will be needed to extract physiological signals saved from DICOM file. Please download this toolbox and add to your own path before running scripts. The toolbox can be downloaded from here (https://www.mathworks.com/matlabcentral/fileexchange/42997-xiangruili-dicm2nii). 


The PDF file (how-to-use-physiology-signal.pdf) includes background of physiology signals and instructions about how to use MATLAB scripts to extract physiological measurements. 
