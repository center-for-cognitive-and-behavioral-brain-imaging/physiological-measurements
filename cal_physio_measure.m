

function [HR, PPG_AMP, RV] = cal_physio_measure(DICOM_NAME)

% Calculate several common physiological measurements from pulse oximetry trace and/or respiration trace. 
% 
% The heart rate (HR) was computed by averaging the time differences of consecutive peaks of 
% the pulse  oximetry trace in a sliding window centered at each TR and converting to units of 
% beats-per-minute (Chang C et al, Neuroimage, 2009).
%
% The respiratory volume (RV) was computed as the standard deviation of the respiratory trace in a 
% sliding window centered at each TR (Chang C et al, Neuroimage, 2009)

% The photoplethysmography (PPG) signal was derived from the pulse oximetry trace. We calculated PPG 
% amplitude (PPG_AMP) by computing the root-mean-square envelop of the pulse oximetry signal and then 
% averaging within each TR, similar to a previous study (Özbay et al., Neuroimage, 2018).
% 
% input:
% -DICOM_NAME: DICOM file name 
%
% Examples: 
% [HR, PPG_AMP, RV] = cal_physio_measure('6434RP_1_s007_trace.dcm');
% [HR, PPG_AMP, RV] = cal_physio_measure('path_to_DICOM_file/6434RP_1_s007_trace.dcm');


%% extract physiological signal trace from DICOM file into MATLAB

% get TR and TR-number from DICOM header (TR unit: seconds; sample rate unit: Hz)
physio_sig_info = dicm_hdr(DICOM_NAME);
tr = (physio_sig_info.RepetitionTime)/1000;
sample_rate = 1000/(physio_sig_info.TemporalResolution);
tr_number = physio_sig_info.NumberOfTemporalPositions;

% test wheather both pulse and respiratory pulse exist
physio_sig = dicm_img(DICOM_NAME);
physio_sig2 = double(permute(physio_sig,[4,1,2,3]));

if strcmp(physio_sig_info.SeriesDescription, 'PulseTraces')
    pulse_trace = physio_sig2;
    respiratory_trace = nan(size(physio_sig2));

elseif strcmp(physio_sig_info.SeriesDescription, 'RespiratoryTraces')
    respiratory_trace = physio_sig2;
    pulse_trace = nan(size(physio_sig2));

elseif strcmp(physio_sig_info.SeriesDescription, 'PulseRespiratoryTraces')
    % first row: pulse oximetry trace; second row: respiration trace
    pulse_trace = physio_sig2(:,1);
    respiratory_trace = physio_sig2(:,2);
end


%% calculate heart rate (method to identify signal peaks from Dr. Xiangrui Li)
% The critical work is to find all peaks. The method here seems reliable:
% 1. detrend and smooth, to be safe;
% 2. Use fft to get heart rate, so the period;
% 3. findpeaks using 60% of period as MinPeakDistance;
% 4. If any peak distance is about twice of period, assign a fake one.

HR = zeros(tr_number,1);
   
for li = 5:tr_number-4
      
    pul = pulse_trace((li-4)*tr*sample_rate+1:(li+4)*tr*sample_rate);

    dt = physio_sig_info.TemporalResolution / 1000;
    n = round(0.08/dt) * 2 + 1;
    pul = smooth(double(pul), n); % remove a lot of small local_max
    pul = detrend(pul); % also demean
    L = numel(pul);
    NFFT = 2^nextpow2(L); % Next power of 2 from length of y
    f = linspace(0,1,NFFT/2+1)' /2/dt;
    Y = fft(pul, NFFT) / L;
    Y = 2 * abs(Y(1:NFFT/2+1));
    n = round(0.08/f(2)) * 2 + 1; % kind of arbitury
    y = smooth(Y, n);
    % plot(f, [Y y]);
    [pks, ind]= findpeaks(y, 'MinPeakDistance', round(0.5/f(2))); % 0.5 Hz apart
    outRange = f(ind)<40/60 | f(ind)>180/60; % [40 180] bpm
    ind(outRange) = []; pks(outRange) = [];

    infor = sprintf('%s, s%03g_%s:', physio_sig_info.PatientName, physio_sig_info.SeriesNumber, physio_sig_info.ProtocolName);
    if isempty(ind)
        warning('%s HeartRate out of range [40 180] bpm.\n', infor);
    return;
    elseif numel(ind) > 1
        [~, i] = max(pks); 
        ind = ind(i);
    end
    HeartRate = 60 * f(ind); % per minute
    d = 1 / f(ind) / dt; % data points distance

    if HeartRate>180 || HeartRate<40
        errorLog('%s Detected heart rate: %.3g', infor, HeartRate);
    return;
    end

    [~, ind]= findpeaks(pul, 'MinPeakDistance', round(d*0.6));
  % [~, ind]= findpeaks(pul, 'MinPeakDistance', round(d*0.6), 'MinPeakHeight', 0);


    if numel(ind)==0
        warning('%s No peaks found in pulse data.', infor);
        return;
    end

    % Insert up to 5 peaks to fix missing recording
    i = diff(ind) > d*1.8;
    if any(i)
        i = find(i);
        n = numel(i);
        fprintf('%s at least %g pulses were missing.\n', infor, n);
        for j = n:-1:1
            later = ind(i(j)+1);
            for k = 1:5 % insert up to 5 peaks
                inserted = later - round(k*d);
                ind = [ind(1:i(j)); inserted; ind(i(j)+1:end)];
                if inserted-ind(i(j))<d*1.5, break; end 
            end
        end
        if any(diff(ind) > d*1.8) % still missing peak? give up
            fprintf('%s inserting 5 peaks did not fix missing peaks.\n', infor);
            return;
        end
    end

    HR(li) = HeartRate;

end

HR(tr_number-3:end) = repmat(HR(tr_number-4),[4,1]);
HR(1:4) = repmat(HR(5),[4,1]);


%% calculate PPG amplitude
PPG_AMP = zeros(tr_number,1);
pulse_trace2 = FT_Filter_wDC(pulse_trace, [0.5 2]*2/(1/(1/sample_rate)));
pulse_amp = envelope(pulse_trace2,1000,'rms');

for lj = 3:tr_number-2
    PPG_AMP(lj) = mean(pulse_amp((lj-2)*sample_rate*tr+1:(lj+2)*sample_rate*tr));
end

PPG_AMP(1:2) = PPG_AMP(3);
PPG_AMP(end-1:end) = PPG_AMP(tr_number-2);


%% calculating respiratory volume
RV = zeros(tr_number,1);

for li = 5:tr_number-4     
    tmp1 = respiratory_trace((li-4)*tr*sample_rate+1:(li+4)*tr*sample_rate);     
    RV(li) = std(tmp1);
end

RV(tr_number-3:end) = repmat(RV(tr_number-4),[4,1]);
RV(1:4) = repmat(RV(5),[4,1]);


end



